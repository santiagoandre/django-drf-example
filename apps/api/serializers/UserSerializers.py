

from rest_framework import serializers

'''ModelSerializer: Tranforma un usuario de Tipo Modelo y lo transforma en un Json'''

from django.contrib.auth import get_user_model
User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # include = ('is_admin','email')
        exclude = []

    