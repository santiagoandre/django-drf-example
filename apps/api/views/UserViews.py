from rest_framework import views
from rest_framework import viewsets
from rest_framework.response import Response
from apps.api.serializers import UserSerializer
from django.contrib.auth import get_user_model
User = get_user_model()

class UserSimpleView(views.APIView):
    def get(self,request):
        users = User.objects.all()
        user_serializer = UserSerializer(users, many = True )
        return Response(user_serializer.data)

class UserModelView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer