
from rest_framework.routers import DefaultRouter
from apps.api.views import UserModelView


router = DefaultRouter()

router.register(r'users',UserModelView,basename = 'users')


urlpatterns = router.urls